import Service from 'store/service'

const state = {
}

const mutations = {
}

const getters = {
}

const actions = {
  async createUser ({ dispatch }, data) {
    const service = new Service(data.host)
    return await service.createUser(data)
  },
  async removeUser ({ dispatch }, data) {
    const service = new Service(data.host)
    return await service.removeUser(data.email)
  },
  async editUser ({ dispatch }, data) {
    const service = new Service(data.host)
    return await service.editUser(data)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

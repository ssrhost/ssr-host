import * as types from '../mutation-types'
import Service from 'store/service'
import moment from 'moment'
import Vue from 'vue'

const state = {
  showLogin: false,
  showQR: null,
  nodes: []
}

const mutations = {
  [types.SHOW_QR] (state, flag) {
    state.showQR = flag
  },
  [types.SHOW_LOGIN] (state, flag) {
    state.showLogin = flag
  },
  [types.UPDATE_PING] (state, data) {
    const { host, ping } = data
    for (const node of state.nodes) {
      if (node.host === host) {
        Vue.set(node, 'ping', ping)
      }
    }
  },
  [types.GET_USERS] (state, data) {
    const { host, users } = data
    for (const node of state.nodes) {
      if (node.host === host) {
        const _users = (users || []).map((user) => {
          user.end = moment.unix(user.end).format('YYYY-MM-DD')
          user.transfer_enable = user.transfer_enable / (1024 * 1024)
          user.d = (user.d / (1024 * 1024)).toFixed(2)
          return user
        })
        Vue.set(node, 'users', _users)
      }
    }
  },
  [types.GET_NODES] (state, data) {
    state.nodes = data || []
  }
}

const getters = {

}

const actions = {
  async toggleQR ({ commit, state }, data) {
    if (data) {
      const service = new Service(data.host)
      const image = await service.getQR(data)
      commit(types.SHOW_QR, 'data:image/png;base64,' + image)
    } else {
      commit(types.SHOW_QR, null)
    }
  },
  toggleLogin ({ commit, state }, data) {
    commit(types.SHOW_LOGIN, data)
  },
  async getUsers ({ commit, state }, data) {
    const service = new Service(data.host)
    const start = +new Date()
    const users = await service.getUsers()
    const end = +new Date()
    const ping = end - start
    commit(types.GET_USERS, {
      host: data.host,
      users
    })
    commit(types.UPDATE_PING, {
      host: data.host,
      ping: ping
    })
  },
  async getNodes ({ commit, state }) {
    const service = new Service()
    const nodes = await service.getNodes()
    commit(types.GET_NODES, nodes)
  },
  async addNode ({ dispatch }, data) {
    const service = new Service()
    return await service.addNode(data)
  },
  async delNode ({ dispatch }, data) {
    const service = new Service()
    return await service.delNode(data.host)
  },
  async login ({ dispatch }, data) {
    const service = new Service()
    return await service.login(data)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

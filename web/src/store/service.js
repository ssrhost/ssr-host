const fetch = window.fetch

export default class Service {
  constructor (host = window.location.hostname) {
    this.host = `http://${host}:8080`
    this.auth = window.localStorage.getItem('auth')
  }
  _request (path, method = 'GET', data = {}) {
    if (!this.auth && path !== 'session') {
      window.store.dispatch('toggleLogin', true)
      return
    }
    return fetch(`${this.host}/${path}`, {
      method: method,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.auth
      },
      body: ['GET', 'DELETE', 'HEAD'].includes(method) ? null : JSON.stringify(data)
    }).then(function (res) {
      if (res.status >= 400) {
        if (res.status === 401) {
          window.store.dispatch('toggleLogin', true)
          return
        }
        return res.json().then((data) => {
          throw data
        })
      }
      return res.json()
    })
  }
  getNodes () {
    return this._request('nodes')
  }
  getUsers () {
    return this._request('users')
  }
  createUser (data) {
    return this._request('user', 'POST', data)
  }
  removeUser (email) {
    return this._request(`user/${email}`, 'DELETE')
  }
  editUser (data) {
    return this._request(`user/${data.email}`, 'PUT', data)
  }
  getQR (data) {
    return this._request(`user/${data.email}/qr`)
  }
  addNode (data) {
    return this._request('node', 'POST', data)
  }
  delNode (host) {
    return this._request(`node/${host}`, 'DELETE')
  }
  async login (data) {
    const ret = await this._request(`session`, 'POST', data)
    if (ret && ret.auth) {
      window.localStorage.setItem('auth', ret.auth)
    }
    return ret
  }
}

#!/bin/sh

rm -rf release/emails release/shadowsocks release/web server config.json npm-debug.log

cd web && npm run build
cd .. && env GOOS=linux GOARCH=amd64 go build -o release/server main.go
cp -r shadowsocks release/.
cp -r emails release/.
cp config-tpl.json release/config.json
cp .gitignore release/.gitignore
rm -rf release/shadowsocks/.git

# env GOOS=linux GOARCH=amd64 go build -o release main.go
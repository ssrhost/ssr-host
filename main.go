package main

import (
	"bitbucket.org/ssrhost/ssr-host/manage"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/go-playground/validator.v9"
	"log"
	"os"
	"net/http"
	"path/filepath"
	"github.com/takama/daemon"
)

var CONFIG *manage.Config

type Service struct {
	daemon.Daemon
}

type (
	Node struct {
		Name string `json:"name" validate:"required"`
		Host string `json:"host" validate:"required"`
		Pass string `json:"pass" validate:"required"`
	}
	User struct {
		Email    string  `json:"email" validate:"required,email"`
		Month    int   `json:"month" validate:"gt=-13"`
		Transfer float64 `json:"transfer" validate:"gt=-1"`
		Limit    int   `json:"limit" validate:"gt=0"`
	}
	Session struct {
		Auth     string  `json:"auth" validate:"required"`
	}
	Validator struct {
		validator *validator.Validate
	}
	Response struct {
		Success bool        `json:"success"`
		Data    interface{} `json:"data"`
	}
)

func (service *Service) Manage() (string, error) {
	usage := "Usage: ./server install | remove | start | stop | status"
  if len(os.Args) > 1 {
    command := os.Args[1]
    switch command {
    case "install":
      return service.Install()
    case "remove":
      return service.Remove()
    case "start":
      return service.Start()
    case "stop":
      return service.Stop()
    case "status":
      return service.Status()
    default:
      return usage, nil
    }
  }
	startServer()
  return usage, nil
}

func NewSession(ctx echo.Context) (*Session, error) {
	session := new(Session)
	if err := ctx.Bind(session); err != nil {
		return nil, err
	}
	if err := ctx.Validate(session); err != nil {
		return nil, err
	}
	return session, nil
}

func NewUser(ctx echo.Context) (*User, error) {
	user := new(User)
	if err := ctx.Bind(user); err != nil {
		return nil, err
	}
	if err := ctx.Validate(user); err != nil {
		return nil, err
	}
	return user, nil
}

func NewNode(ctx echo.Context) (*Node, error) {
	node := new(Node)
	if err := ctx.Bind(node); err != nil {
		return nil, err
	}
	if err := ctx.Validate(node); err != nil {
		return nil, err
	}
	return node, nil
}

func Auth(ctx echo.Context) error {
	pass := ctx.Request().Header.Get("Authorization")
	if pass == CONFIG.Password {
		return nil
	}
	return ctx.JSON(http.StatusUnauthorized, nil)
}

func (cv *Validator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func init() {
	// manage.Command("pkill -9 python")
	go manage.CommandWait("python server.py m")
}

func startServer() {
	server := echo.New()
	server.Validator = &Validator{validator: validator.New()}
	server.Use(middleware.CORS())
	server.Static("/", filepath.Join(manage.DIR, "web"))

	server.POST("/session", func(ctx echo.Context) error {
		session, err := NewSession(ctx)
		if err != nil {
			return ctx.JSON(http.StatusBadRequest, Response{false, "登陆失败，请正确填写参数"})
		}
		if session.Auth == CONFIG.Password {
			return ctx.JSON(http.StatusOK, session)
		}
		return ctx.JSON(http.StatusBadRequest, Response{false, "密码错误，登陆失败"})
	})

	server.GET("/users", func(ctx echo.Context) error {
		if err := Auth(ctx); err != nil {
			return err
		}
		users := manage.LsUser()
		return ctx.JSON(http.StatusOK, users)
	})

	server.GET("/nodes", func(ctx echo.Context) error {
		if err := Auth(ctx); err != nil {
			return err
		}
		nodes := manage.LsNode()
		return ctx.JSON(http.StatusOK, nodes)
	})

	server.GET("/user/:email/qr", func(ctx echo.Context) error {
		if err := Auth(ctx); err != nil {
			return err
		}
		email := ctx.Param("email")
		if ret := manage.GetQR(email, ""); ret != nil {
			return ctx.JSON(http.StatusOK, ret)
		}
		return ctx.JSON(http.StatusBadRequest, Response{false, "获取二维码失败"})
	})

	server.POST("/user", func(ctx echo.Context) error {
		if err := Auth(ctx); err != nil {
			return err
		}
		user, err := NewUser(ctx)
		if err != nil {
			log.Println(err)
			return ctx.JSON(http.StatusBadRequest, Response{false, "创建失败，请正确填写参数"})
		}
		if ret := manage.AddUser(user.Email, user.Month, user.Transfer, user.Limit); !ret {
			return ctx.JSON(http.StatusBadRequest, Response{false, "创建失败，用户已存在"})
		}
		return ctx.JSON(http.StatusOK, nil)
	})

	server.POST("/node", func(ctx echo.Context) error {
		if err := Auth(ctx); err != nil {
			return err
		}
		node, err := NewNode(ctx)
		if err != nil {
			log.Println(err)
			return ctx.JSON(http.StatusBadRequest, Response{false, "创建失败，请正确填写参数"})
		}
		if ret := manage.AddNode(node.Name, node.Host, node.Pass); !ret {
			return ctx.JSON(http.StatusBadRequest, Response{false, "节点创建失败，请确认参数正确"})
		}
		return ctx.JSON(http.StatusOK, nil)
	})

	server.DELETE("/node/:host", func(ctx echo.Context) error {
		if err := Auth(ctx); err != nil {
			return err
		}
		host := ctx.Param("host")
		if ret := manage.DelNode(host); !ret {
			return ctx.JSON(http.StatusBadRequest, Response{false, "删除失败"})
		}
		return ctx.JSON(http.StatusOK, nil)
	})

	server.DELETE("/user/:email", func(ctx echo.Context) error {
		if err := Auth(ctx); err != nil {
			return err
		}
		email := ctx.Param("email")
		if ret := manage.DelUser(email); !ret {
			return ctx.JSON(http.StatusBadRequest, Response{false, "删除失败"})
		}
		return ctx.JSON(http.StatusOK, nil)
	})

	server.PUT("/user/:email", func(ctx echo.Context) error {
		if err := Auth(ctx); err != nil {
			return err
		}
		email := ctx.Param("email")
		user, err := NewUser(ctx)
		if err != nil {
			log.Println(err)
			return ctx.JSON(http.StatusBadRequest, Response{false, "编辑失败，请正确填写参数"})
		}
		if ret := manage.EditUser(email, int(user.Month), int(user.Limit), user.Transfer == 0); !ret {
			return ctx.JSON(http.StatusBadRequest, Response{false, "编辑失败"})
		}
		return ctx.JSON(http.StatusOK, nil)
	})

	manage.Assert(server.Start(":8080"))
}

func main() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
  manage.Assert(err)
  manage.DIR = dir
	manage.Init()
	CONFIG = manage.GetConfig()
	if CONFIG == nil {
		log.Fatal("Please confim config.json is valid.")
	}

	srv, err := daemon.New("ssr-host", "An management tool", "")
	manage.Assert(err)
	service := &Service{srv}
	status, err := service.Manage()
	if err != nil {
		log.Println(status, "\nError: ", err)
		os.Exit(1)
	}
	log.Println(status)
}

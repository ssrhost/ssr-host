package manage

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/skip2/go-qrcode"
	"gopkg.in/gomail.v2"
)

const (
	SSR_MGR = "python mujson_mgr.py"
)

var DIR string

var tpls map[string]*template.Template

type User struct {
	Name     string  `json:"user"`
	Port     int     `json:"port"`
	Pass     string  `json:"passwd"`
	End      int     `json:"end"`
	Status   string  `json:"status"`
	Transfer float64 `json:"transfer_enable"`
	Limit    string  `json:"protocol_param"`
	Data     float64 `json:"d"`
	Method   string  `json:"method"`
	Qr       string  `json:"qr"`
}

type Users []User

type Node struct {
	Name string `json:"name"`
	Host string `json:"host"`
	Pass string `json:"pass"`
}

type Config struct {
	Nodes    []Node `json:"nodes"`
	Password string `json:"password"`
}

func (users Users) Len() int           { return len(users) }
func (users Users) Swap(i, j int)      { users[i], users[j] = users[j], users[i] }
func (users Users) Less(i, j int) bool { return users[i].Port < users[j].Port }

func include(src string, sub string) bool {
	return strings.Contains(src, sub)
}

func Warn(msg interface{}) {
	log.Println(msg)
}

func Assert(err error) bool {
	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

func initTpl(name string) {
	path := filepath.Join(DIR, "emails/"+name+".html")
	html, err := ioutil.ReadFile(path)
	Assert(err)
	tpl := template.Must(template.New("new").Parse(string(html)))
	tpls[name] = tpl
}

func mailTpl(name string, data interface{}) string {
	tpl := tpls[name]
	var ret bytes.Buffer
	err := tpl.Execute(&ret, data)
	Assert(err)
	return ret.String()
}

func sendMail(to string, tpl string, data interface{}, attach string) bool {
	mail := gomail.NewMessage()
	mail.SetHeader("From", "noreply@houchangcun.cc")
	mail.SetHeader("To", to)
	mail.SetHeader("Subject", "SSR管理平台通知")
	mail.SetBody("text/html", mailTpl(tpl, data))

	if attach != "" {
		mail.Attach(attach)
	}

	conn := gomail.NewDialer(
		"smtp.exmail.qq.com",
		465,
		"noreply@houchangcun.cc",
		"fuckQiang1024",
	)
	conn.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	if err := conn.DialAndSend(mail); err != nil {
		Warn(err)
		return false
	}
	return true
}

func setExpire(name string, soon bool) bool {
	status := "expired"
	if soon {
		status = "will_expire"
	}
	ret := command("-e", "-u", name, "-s", status)
	if include(ret, "new user info") {
		return true
	}
	return false
}

func getStatus(name string) string {
	user := FindUser(name)
	if user != nil && user.Status != "" {
		return user.Status
	}
	return ""
}

func genPort() string {
	users := LsUser()
	if len(users) == 0 {
		return "443"
	}
	last := users[len(users)-1]
	return strconv.Itoa(last.Port + 1)
}

func command(args ...string) string {
	str := append([]string{SSR_MGR}, args...)
	return Command(strings.Join(str, " "))
}

func Command(str string) string {
	cmd := exec.Command("sh", "-c", str)
	cmd.Dir = filepath.Join(DIR, "shadowsocks")
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		return stderr.String()
	}
	stdoutStr := out.String()
	stderrStr := stderr.String()
	if stdoutStr != "" {
		Warn(stdoutStr)
	}
	if stderrStr != "" {
		Warn(stderrStr)
	}
	return stdoutStr
}

func CommandWait(str string) {
	cmd := exec.Command("sh", "-c", str)
	cmd.Dir = filepath.Join(DIR, "shadowsocks")

	stdoutPipe, err := cmd.StdoutPipe()
	Assert(err)
	stderrPipe, err := cmd.StderrPipe()
	Assert(err)
	go io.Copy(os.Stdout, stdoutPipe)
	go io.Copy(os.Stderr, stderrPipe)

	if cmd.Run() != nil {
		cmd.Wait()
	}
}

func EditUser(name string, month int, limit int, clearTransfer bool) bool {
	user := FindUser(name)
	if user == nil {
		return false
	}
	var ret string
	if month != 0 {
		end := strconv.FormatInt(time.Unix(int64(user.End), 0).AddDate(0, month, 0).Unix(), 10)
		ret = command("-e", "-u", name, "-b", end)
	}
	if limit > 0 {
		ret = command("-e", "-u", name, "-G", strconv.FormatInt(int64(limit), 10))
	}
	if clearTransfer {
		ret = command("-e", "-u", name, "-i", "0")
	}
	if include(ret, "new user info") {
		return true
	}
	return false
}

func AddUser(name string, month int, transfer float64, limit int) bool {
	port := genPort()
	end := strconv.FormatInt(time.Now().AddDate(0, int(month), 0).Unix(), 10)
	if _, err := os.Stat(filepath.Join(DIR, "shadowsocks/mudb.json")); os.IsNotExist(err) {
		if err := ioutil.WriteFile(filepath.Join(DIR, "shadowsocks/mudb.json"), []byte("[]"), 0777); err != nil {
			return false
		}
	}
	ret := command(
		"-a", "-u", name,
		"-p", port,
		"-b", end,
		"-t", strconv.FormatFloat(transfer, 'E', -1, 64),
		"-O", "+4",
		"-G", strconv.FormatInt(int64(limit), 10),
		"-m", "a0",
	)
	if include(ret, "### add user info") {
		user := FindUser(name)
		image := GenQR(user.Name, "")
		go func() {
			defer os.Remove(image)
			sendMail(name, "new", user, image)
		}()
		return true
	}
	return false
}

func DelUser(name string) bool {
	ret := command("-d", "-u", name)
	if include(ret, "delete user") {
		return true
	}
	return false
}

func LsUser() Users {
	content, err := ioutil.ReadFile(filepath.Join(DIR, "shadowsocks/mudb.json"))
	if err != nil {
		return nil
	}
	var users Users
	err = json.Unmarshal(content, &users)
	if err != nil {
		return nil
	}
	sort.Sort(users)
	return users
}

func FindUser(name string) *User {
	users := LsUser()
	for _, user := range users {
		if user.Name == name {
			return &user
		}
	}
	return nil
}

func GetQR(name string, ip string) []byte {
	user := FindUser(name)
	if user != nil {
		if ip == "" {
			ip = "104.155.197.17"
		}
		data := user.Method + ":" + user.Pass + "@" + ip + ":" + strconv.Itoa(user.Port)
		base := base64.StdEncoding.EncodeToString([]byte(data))
		image, err := qrcode.Encode("ss://"+base, qrcode.Medium, 256)
		if err == nil {
			return image
		}
	}
	return nil
}

func GenQR(name string, ip string) string {
	user := FindUser(name)
	if user != nil {
		if ip == "" {
			ip = "104.155.197.17"
		}
		data := user.Method + ":" + user.Pass + "@" + ip + ":" + strconv.Itoa(user.Port)
		base := base64.StdEncoding.EncodeToString([]byte(data))
		file, err := ioutil.TempFile("", "SSR_QR_")
		if err != nil {
			return ""
		}
		pngName := file.Name() + ".png"
		if err := os.Rename(file.Name(), pngName); err != nil {
			return ""
		}
		if err := qrcode.WriteFile("ss://"+base, qrcode.Medium, 256, pngName); err == nil {
			return pngName
		}
	}
	return ""
}

func ClsUser() {
	users := LsUser()
	for _, user := range users {
		DelUser(user.Name)
	}
}

func GetConfig() *Config {
	if _, err := os.Stat(filepath.Join(DIR, "config.json")); os.IsNotExist(err) {
		if err := ioutil.WriteFile(filepath.Join(DIR, "config.json"), []byte(`{"nodes":[],"password":"ssrhost"}`), 0777); err != nil {
			return nil
		}
	}
	content, err := ioutil.ReadFile(filepath.Join(DIR, "config.json"))
	if err != nil {
		return nil
	}
	var config Config
	err = json.Unmarshal(content, &config)
	if err != nil {
		return nil
	}
	return &config
}

func SetConfig(config *Config) bool {
	ret, err := json.Marshal(config)
	if err != nil {
		return false
	}
	if err := ioutil.WriteFile(filepath.Join(DIR, "config.json"), ret, 0777); err != nil {
		return false
	}
	return true
}

func AddNode(name string, host string, pass string) bool {
	config := GetConfig()
	if config != nil {
		config.Nodes = append(config.Nodes, Node{name, host, pass})
		if SetConfig(config) {
			return true
		}
	}
	return false
}

func DelNode(host string) bool {
	config := GetConfig()
	if config != nil {
		for idx, node := range config.Nodes {
			if node.Host == host {
				config.Nodes = append(config.Nodes[:idx], config.Nodes[idx+1:]...)
				if SetConfig(config) {
					return true
				}
			}
		}
	}
	return false
}

func LsNode() []Node {
	config := GetConfig()
	if config != nil {
		return config.Nodes
	}
	return nil
}

func Cron() {
	wait := make(chan bool)
	time.AfterFunc(3*time.Second, func() {
		users := LsUser()
		for _, user := range users {
			now := time.Now().Unix()
			expire := int64(user.End)
			willExpire := time.Unix(expire, 0).AddDate(0, 0, -3).Unix()
			status := getStatus(user.Name)
			if now >= willExpire && status == "" {
				sendMail(user.Name, "will_expire", user, "")
				setExpire(user.Name, true)
			}
			if now >= expire && (status == "" || status == "will_expire") {
				sendMail(user.Name, "expired", user, "")
				DelUser(user.Name)
			}
		}
		Cron()
	})
	<-wait
}

func Init() {
	tpls = make(map[string]*template.Template)
	initTpl("expired")
	initTpl("new")
	initTpl("will_expire")
	go Cron()
}
